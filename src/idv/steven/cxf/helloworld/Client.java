package idv.steven.cxf.helloworld;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {
	public static void main(String[] args) {
		System.out.println("Starting Client");
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"classpath:client-beans.xml"});
		
		HelloWorld helloWorld = (HelloWorld) context.getBean("client");
		System.out.println(helloWorld.sayHello("Steven "));
	}
}
